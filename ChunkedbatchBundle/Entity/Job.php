<?php

namespace ChunkedBatch\ChunkedbatchBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity()
 * @ORM\Table(name="chunked_jobs")
 */
class Job
{
    public const STATUS_NEW = 'new';

    public const STATUS_RUNNING = 'running';

    public const STATUS_SUCCESSFUL = 'successful';

    public const STATUS_FAILED = 'failed';

    public const STATUS_CLEARED = 'cleared';

    /**
     * @var int
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * @ORM\Column(type="string", length=255)
     */
    protected $type;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    protected $title;

    /**
     * @ORM\Column(type="datetime")
     */
    protected $createdAt;

    /**
     * @ORM\Column(type="string", length=255)
     */
    protected $createdBy;

    /**
     * @ORM\Column(type="datetime", nullable=true)
     */
    protected $executedAt;

    /**
     * @ORM\Column(type="string", nullable=false, length=255)
     */
    protected $status = self::STATUS_NEW;

    /**
     * @ORM\Column(type="array")
     */
    protected $parameters = [];

    /**
     * @ORM\Column(type="text", nullable=true)
     */
    protected $result;

    /**
     * @ORM\Column(type="datetime", nullable=true)
     */
    protected $finishedAt;

    /**
     * @ORM\Column(type="text", nullable=true)
     */
    protected $error;

    /**
     * @ORM\Column(type="integer", nullable=true)
     */
    protected $resultSize;

    /**
     * @ORM\Column(type="integer", nullable=true)
     */
    protected $progress = 0;

    /**
     * Job constructor.
     * @param string $type
     * @param string $title
     * @param string $createdBy
     */
    public function __construct(string $type, string $title, string $createdBy)
    {
        $this->setType($type);
        $this->setTitle($title);
        $this->setCreatedBy($createdBy);
        if (!$this->getCreatedAt()) {
            $this->setCreatedAt(new \DateTime());
        }
    }

    /**
     * @return int
     */
    public function getId(): int
    {
        return $this->id;
    }

    /**
     * @param int $id
     * @return Job
     */
    public function setId(int $id): Job
    {
        $this->id = $id;
        return $this;
    }

    /**
     * @return string|null
     */
    public function getType(): ?string
    {
        return $this->type;
    }

    /**
     * @param string $type
     * @return $this
     */
    public function setType(string $type): self
    {
        $this->type = $type;
        return $this;
    }

    /**
     * @return \DateTimeInterface|null
     */
    public function getCreatedAt(): ?\DateTimeInterface
    {
        return $this->createdAt;
    }

    /**
     * @param \DateTimeInterface $createdAt
     * @return $this
     */
    public function setCreatedAt(\DateTimeInterface $createdAt): self
    {
        $this->createdAt = $createdAt;
        return $this;
    }

    /**
     * @return \DateTimeInterface|null
     */
    public function getExecutedAt(): ?\DateTimeInterface
    {
        return $this->executedAt;
    }

    /**
     * @param \DateTimeInterface|null $executedAt
     * @return $this
     */
    public function setExecutedAt(?\DateTimeInterface $executedAt): self
    {
        $this->executedAt = $executedAt;
        return $this;
    }

    /**
     * @return string
     */
    public function getStatus(): string
    {
        return $this->status;
    }

    /**
     * @param string $status
     * @return $this
     */
    public function setStatus(string $status): self
    {
        $this->status = $status;
        return $this;
    }

    /**
     * @return array|null
     */
    public function getParameters(): ?array
    {
        return $this->parameters;
    }

    /**
     * @param array $parameters
     * @return $this
     */
    public function setParameters(array $parameters): self
    {
        $this->parameters = $parameters;
        return $this;
    }

    /**
     * @param string $key
     * @return mixed|null
     */
    public function getParameter(string $key)
    {
        if (!array_key_exists($key, $this->getParameters())) {
            return null;
        }
        return $this->getParameters()[$key];
    }

    /**
     * @return string|null
     */
    public function getResult(): ?string
    {
        return $this->result;
    }

    /**
     * @param string|null $result
     * @return $this
     */
    public function setResult(?string $result): self
    {
        $this->result = $result;
        return $this;
    }

    /**
     * @return string
     */
    public function getResultBasename(): string
    {
        if (!$this->getResult()) {
            return '';
        }
        return basename($this->getResult());
    }

    /**
     * @return \DateTimeInterface|null
     */
    public function getFinishedAt(): ?\DateTimeInterface
    {
        return $this->finishedAt;
    }

    /**
     * @param \DateTimeInterface|null $finishedAt
     * @return $this
     */
    public function setFinishedAt(?\DateTimeInterface $finishedAt): self
    {
        $this->finishedAt = $finishedAt;
        return $this;
    }

    /**
     * @return string|null
     */
    public function getError(): ?string
    {
        return $this->error;
    }

    /**
     * @param string|null $error
     * @return $this
     */
    public function setError(?string $error): self
    {
        $this->error = $error;
        return $this;
    }

    /**
     * @return string|null
     */
    public function getTitle(): ?string
    {
        return $this->title;
    }

    /**
     * @param string|null $title
     * @return $this
     */
    public function setTitle(?string $title): self
    {
        $this->title = $title;
        return $this;
    }

    /**
     * @return string|null
     */
    public function getCreatedBy(): ?string
    {
        return $this->createdBy;
    }

    /**
     * @param string $createdBy
     * @return $this
     */
    public function setCreatedBy(string $createdBy): self
    {
        $this->createdBy = $createdBy;
        return $this;
    }

    /**
     * @return int|null
     */
    public function getResultSize(): ?int
    {
        return $this->resultSize;
    }

    /**
     * @param int|null $resultSize
     * @return $this
     */
    public function setResultSize(?int $resultSize): self
    {
        $this->resultSize = $resultSize;
        return $this;
    }

    /**
     * @return int|null
     */
    public function getProgress(): ?int
    {
        return $this->progress;
    }

    /**
     * @param $progress
     */
    public function setProgress($progress): void
    {
        $this->progress = $progress;
    }
}
