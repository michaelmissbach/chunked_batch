<?php

namespace ChunkedBatch\ChunkedbatchBundle\Factory;

use ChunkedBatch\ChunkedbatchBundle\Abstracts\ChunkedJob;

/**
 * Class JobFactory
 * @package ChunkedBatch\ChunkedbatchBundle\Factory
 */
class JobFactory
{
    /**
     * @var array
     */
    protected $jobs = [];

    /**
     * @param ChunkedJob $job
     */
    public function addJob(ChunkedJob $job): void
    {
        $this->jobs[$job::getType()] = $job;
    }

    /**
     * @return array
     */
    public function get(): array
    {
        return $this->jobs??[];
    }

    /**
     * @return \Generator
     */
    public function getYielded(): \Generator
    {
        foreach($this->jobs??[] as $key=>$value) {
            yield $key=>$value;
        }
    }

    /**
     * @param $type
     * @return bool
     */
    public function existsType($type): bool
    {
        return array_key_exists($type,$this->jobs);
    }

    /**
     * @param string $type
     * @return ChunkedJob|null
     */
    public function getByType(string $type): ?ChunkedJob
    {
        return $this->jobs[$type]??null;
    }
}
