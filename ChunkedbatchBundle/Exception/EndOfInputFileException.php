<?php

namespace ChunkedBatch\ChunkedbatchBundle\Exception;

/**
 * Class EndOfInputFileException
 * @package ChunkedBatch\ChunkedbatchBundle\Exception
 */
class EndOfInputFileException extends \Exception
{
}