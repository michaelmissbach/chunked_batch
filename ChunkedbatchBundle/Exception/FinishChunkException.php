<?php

namespace ChunkedBatch\ChunkedbatchBundle\Exception;

/**
 * Class FinishChunkException
 * @package ChunkedBatch\ChunkedbatchBundle\Exception
 */
class FinishChunkException extends \Exception
{
}