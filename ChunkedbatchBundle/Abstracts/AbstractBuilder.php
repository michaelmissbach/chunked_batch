<?php

namespace ChunkedBatch\ChunkedbatchBundle\Abstracts;

/**
 * Class AbstractBuilder
 * @package ChunkedBatch\ChunkedbatchBundle\Abstracts
 */
class AbstractBuilder
{
    /**
     * @var array
     */
    protected $builderPayload;

    /**
     * AbstractBuilder constructor.
     */
    protected function __construct()
    {
        $this->builderPayload = [];
    }

    /**
     * @return $this
     */
    public static function create()
    {
        return new static();
    }

    /**
     * @internal
     * @return \Generator
     */
    public function getYielded(): \Generator
    {
        foreach ($this->builderPayload as $payload) {
            yield $payload;
        }
    }

    /**
     * @internal
     * @return array
     */
    public function getAll(): array
    {
        return $this->builderPayload;
    }

     /**
     * @internal
     * @return int
     */
    public function count(): int
    {
        return count($this->builderPayload);
    }

    /**
     * @return bool
     */
    public function clear(): bool
    {
        $this->builderPayload = [];
        return true;
    }

    /**
     * @param $payload
     * @return bool
     */
    public function add($payload): bool
    {
        $this->builderPayload[] = $payload;

        return true;
    }
}
