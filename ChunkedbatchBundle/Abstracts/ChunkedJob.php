<?php

namespace ChunkedBatch\ChunkedbatchBundle\Abstracts;

use ChunkedBatch\ChunkedbatchBundle\Interfaces\IChunkedJob;

/**
 * Class ChunkedJob
 * @package ChunkedBatch\ChunkedbatchBundle\Abstracts
 */
abstract class ChunkedJob implements IChunkedJob
{
    /**
     *
     */
    public function beforeChunkStageIndex(): void
    {
    }

    /**
     * @param $result
     * @return array
     */
    public function afterChunkStageIndex(&$result): void
    {
    }

    /**
     *
     */
    public function afterSetFinish(): void
    {
    }
}