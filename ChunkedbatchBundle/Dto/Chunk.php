<?php

namespace ChunkedBatch\ChunkedbatchBundle\Dto;

/**
 * Class Chunk
 * @package ChunkedBatch\ChunkedbatchBundle\Dto
 */
class Chunk
{
    /**
     * @var string
     */
    protected $type;

    /**
     * @var int
     */
    protected $jobId;

    /**
     * @var string
     */
    protected $stage;

    /**
     * @var int
     */
    protected $stageNumber;

    /**
     * @var int
     */
    protected $counter = 0;

    /**
     * @var int
     */
    protected $maxCounter = 0;

    /**
     * @var bool
     */
    protected $finish = false;

    /**
     * @var bool
     */
    protected $failure = false;

    /**
     * @var array
     */
    protected $messages = [];

    /**
     * @var int
     */
    protected $maxExecutionInSeconds = 120;

    /**
     * @var int
     */
    protected $maxMemoryUsageInBytes = 512000000;

    /**
     * @var bool
     */
    protected $debug = false;

    /**
     * Chunk constructor.
     * @param array $options
     */
    protected function __construct(){}

    /**
     * @return self
     */
    public static function create(): self
    {
        return new self();
    }

    /**
     * @param string|null $json
     * @return self
     */
    public static function fromJson(string $json = null): self
    {
        return self::fromArray(json_decode($json,true)??[]);
    }

    /**
     * @param array $input
     * @return self
     */
    public static function fromArray(array $input = []): self
    {
        $instance = self::create();
        foreach (get_object_vars($instance) as $key=>$value) {
            if (array_key_exists($key,$input)) {
                $method = sprintf('set%s',ucfirst($key));
                if (method_exists($instance,$method)) {
                    $instance->$method($input[$key]);
                }
            }
        }
        return $instance;
    }

    /**
     * @return array
     */
    public function toArray(): array
    {
        return array_filter(get_object_vars($this));
    }

    /**
     * @return string
     */
    public function toJson(): string
    {
        return json_encode($this->toArray());
    }

    /**
     * @return string
     */
    public function getType(): string
    {
        return $this->type;
    }

    /**
     * @param string $type
     * @return Chunk
     */
    public function setType(string $type): Chunk
    {
        $this->type = $type;
        return $this;
    }

    /**
     * @return int
     */
    public function getJobId(): int
    {
        return $this->jobId;
    }

    /**
     * @param int $jobId
     * @return Chunk
     */
    public function setJobId(int $jobId): Chunk
    {
        $this->jobId = $jobId;
        return $this;
    }

    /**
     * @return string
     */
    public function getStage(): ?string
    {
        return $this->stage;
    }

    /**
     * @param string $stage
     * @return Chunk
     */
    public function setStage(string $stage): Chunk
    {
        $this->stage = $stage;
        return $this;
    }

    /**
     * @return int
     */
    public function getStageNumber(): int
    {
        return $this->stageNumber;
    }

    /**
     * @param int $stageNumber
     * @return Chunk
     */
    public function setStageNumber(int $stageNumber): Chunk
    {
        $this->stageNumber = $stageNumber;
        return $this;
    }

    /**
     * @return bool
     */
    public function isFinish(): bool
    {
        return $this->finish;
    }

    /**
     * @param bool $finish
     * @return Chunk
     */
    public function setFinish(bool $finish): Chunk
    {
        $this->finish = $finish;
        return $this;
    }

    /**
     * @return int
     */
    public function getCounter(): int
    {
        return $this->counter;
    }

    /**
     * @param int $counter
     * @return Chunk
     */
    public function setCounter(int $counter): Chunk
    {
        $this->counter = $counter;
        return $this;
    }

    /**
     * @return Chunk
     */
    public function iterateCounter(): Chunk
    {
        $this->counter++;
        return $this;
    }

    /**
     * @return int
     */
    public function getMaxCounter(): int
    {
        return $this->maxCounter;
    }

    /**
     * @param int $maxCounter
     * @return Chunk
     */
    public function setMaxCounter(int $maxCounter): Chunk
    {
        $this->maxCounter = $maxCounter;
        return $this;
    }

    /**
     * @return bool
     */
    public function isFailure(): bool
    {
        return $this->failure;
    }

    /**
     * @param bool $failure
     * @return Chunk
     */
    public function setFailure(bool $failure): Chunk
    {
        $this->failure = $failure;
        return $this;
    }

    /**
     * @return []
     */
    public function getMessagesRaw(): array
    {
        return $this->messages;
    }

    /**
     * @return string
     */
    public function getMessagesAsString(): string
    {
        return implode(PHP_EOL,$this->messages);
    }

    /**
     * @param mixed $message
     * @return Chunk
     */
    public function addMessage($message)
    {
        $this->messages[] = $message;
        return $this;
    }

    /**
     * @param array $messages
     * @return Chunk
     */
    public function setMessages(array $messages): Chunk
    {
        $this->messages = $messages;
        return $this;
    }

    /**
     * @return int
     */
    public function getMaxExecutionInSeconds(): int
    {
        return $this->maxExecutionInSeconds;
    }

    /**
     * @param int $maxExecutionInSeconds
     * @return Chunk
     */
    public function setMaxExecutionInSeconds(int $maxExecutionInSeconds): Chunk
    {
        $this->maxExecutionInSeconds = $maxExecutionInSeconds;
        return $this;
    }

    /**
     * @return int
     */
    public function getMaxMemoryUsageInBytes(): int
    {
        return $this->maxMemoryUsageInBytes;
    }

    /**
     * @param int $maxMemoryUsageInBytes
     * @return Chunk
     */
    public function setMaxMemoryUsageInBytes(int $maxMemoryUsageInBytes): Chunk
    {
        $this->maxMemoryUsageInBytes = $maxMemoryUsageInBytes;
        return $this;
    }

    /**
     * @return bool
     */
    public function isDebug(): bool
    {
        return $this->debug;
    }

    /**
     * @param bool $debug
     * @return Chunk
     */
    public function setDebug(bool $debug): Chunk
    {
        $this->debug = $debug;
        return $this;
    }
}
