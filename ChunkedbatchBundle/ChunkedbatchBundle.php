<?php

namespace ChunkedBatch\ChunkedbatchBundle;

use ChunkedBatch\ChunkedbatchBundle\DependencyInjection\Compiler\JobControllerPass;
use Symfony\Component\DependencyInjection\ContainerBuilder;
use Symfony\Component\HttpKernel\Bundle\Bundle;

/**
 * Class ChunkedbatchBundle
 * @package ChunkedBatch\ChunkedbatchBundle
 */
class ChunkedbatchBundle extends Bundle
{
    /**
     * @param ContainerBuilder $container
     */
    public function build(ContainerBuilder $container)
    {
        parent::build($container);

        $container->addCompilerPass(new JobControllerPass());
    }
}
