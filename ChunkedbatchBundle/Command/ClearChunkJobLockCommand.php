<?php

namespace ChunkedBatch\ChunkedbatchBundle\Command;

use ChunkedBatch\ChunkedbatchBundle\Entity\Job;
use ChunkedBatch\ChunkedbatchBundle\Factory\JobFactory;
use Doctrine\Persistence\ManagerRegistry;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

/**
 * Class ClearChunkJobLockCommand
 * @package ChunkedBatch\ChunkedbatchBundle\Command
 */
class ClearChunkJobLockCommand extends Command
{
    /**
     * @var string
     */
    protected static $defaultName = 'chunked:jobs:clear-lock';

    /**
     * @var JobFactory
     */
    protected $factory;

    /**
     * @var ManagerRegistry
     */
    protected $doctrine;

    /**
     * AddChunkJobCommand constructor.
     */
    public function __construct(ManagerRegistry $doctrine, JobFactory $factory)
    {
        parent::__construct();

        $this->doctrine = $doctrine;
        $this->factory = $factory;
    }

    /**
     *
     */
    protected function configure()
    {
        $this
            ->setDescription('Clears chunk-job lock.')
        ;
    }

    /**
     * @param InputInterface $input
     * @param OutputInterface $output
     * @return int|null
     * @throws \Exception
     */
    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $repo = $this->doctrine->getRepository(Job::class);
        $jobs = $repo->findBy(['status' => Job::STATUS_RUNNING]);

        $counter = 0;
        $manager =  $this->doctrine->getManager();

        /** @var Job $job */
        foreach ($jobs as $job) {
            $job->setStatus(Job::STATUS_CLEARED);
            $manager->persist($job);
            $counter++;
        }

        $manager->flush();

        $output->writeln(sprintf('Done. %s cleared.',$counter));

        return 0;
    }
}
