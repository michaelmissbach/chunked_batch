<?php

namespace ChunkedBatch\ChunkedbatchBundle\Command;

use ChunkedBatch\ChunkedbatchBundle\Dto\Chunk;
use ChunkedBatch\ChunkedbatchBundle\Entity\Job;
use ChunkedBatch\ChunkedbatchBundle\Factory\JobFactory;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Helper\ProgressBar;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Style\SymfonyStyle;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Process\Process;

/**
 * Class ChunkJobsCommand
 * @package ChunkedBatch\ChunkedbatchBundle\Command
 */
class ChunkJobsCommand extends Command
{
    /**
     * @var string
     */
    protected static $defaultName = 'chunked:jobs:run';

    /**
     * @var EntityManagerInterface
     */
    protected $em;

    /**
     * @var JobFactory
     */
    protected $factory;

    /**
     * ChunkJobsCommand constructor.
     * @param EntityManagerInterface $em
     * @param JobFactory $factory
     */
    public function __construct(EntityManagerInterface $em, JobFactory $factory)
    {
        parent::__construct();

        $this->em = $em;
        $this->em->getConnection()->getConfiguration()->setSQLLogger(null);
        $this->factory = $factory;
    }

    /**
     *
     */
    protected function configure()
    {
        $this
            ->setDescription('Executes the next job')
            ->addOption('phpexecutable', 'pe',InputOption::VALUE_OPTIONAL, 'Path to php executable',null)
            ->addOption('approot', 'ar',InputOption::VALUE_OPTIONAL, 'App root path',null)
            ->addOption('memory', 'me',InputOption::VALUE_REQUIRED, 'Max memory in bytes', 512000000)
            ->addOption('execution', 'ex',InputOption::VALUE_REQUIRED, 'Max execution in seconds', 120)
            ->addOption('debug', 'de',InputOption::VALUE_REQUIRED, 'Debug prints and logs', 0)
        ;
    }

    /**
     * @param InputInterface $input
     * @param OutputInterface $output
     * @return int|null
     * @throws \Exception
     */
    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $io = new SymfonyStyle($input, $output);

        $repo = $this->em->getRepository(Job::class);

        $jobs = $repo->findBy(['status' => Job::STATUS_RUNNING], ['createdAt' => 'asc']);
        if (count($jobs)) {
            $output->writeln('There is already a running task. Skip.');
            return 0;
        }

        /** @var Job $job */
        $job = $repo->findOneBy(['status' => Job::STATUS_NEW], ['createdAt' => 'asc']);
        if (!$job) {
            $output->writeln('No job found. All done.');
            return 0;
        }

        if (!$this->factory->existsType($job->getType())) {
            throw new \RuntimeException(
                'The job to execute is from type "' . $job->getType() .
                '", which is no available job type!'
            );
        }

        $job->setStatus(Job::STATUS_RUNNING);
        $job->setExecutedAt(new \DateTime());

        $this->em->persist($job);
        $this->em->flush();

        try {
            $this->writeSeparator($output);
            $output->writeln(sprintf('Execute chunked job id "%s" from type "%s".',$job->getId(),$job->getType()));
            $this->writeSeparator($output);
            $output->writeln('');

            $actionResponse = $this->handleChunkable($job,$input,$output);

            if ($actionResponse) {
                // SUCCESS
                $job->setStatus(Job::STATUS_SUCCESSFUL);
                $job->setFinishedAt(new \DateTime());
                $job->setProgress(100);
                $io->success('Job successfully completed!');
            } else {
                throw new \Exception('Job failed!');
            }
        } catch(\Throwable | \Exception $e) {
            $job->setStatus(Job::STATUS_FAILED);
            $io->error(sprintf('""%s" in "%s" at line %s',$e->getMessage(),$e->getFile(),$e->getLine()));
        }

        $this->em->persist($job);
        $this->em->flush();

        return 0;
    }

    /**
     * @param Job $job
     * @param InputInterface $input
     * @param OutputInterface $output
     * @return Response
     */
    protected function handleChunkable(Job $job,InputInterface $input, OutputInterface $output): bool
    {
        $chunk = Chunk::create();
        $chunk->setType($job->getType())->setJobId($job->getId());

        $chunk->setMaxMemoryUsageInBytes($input->getOption('memory'));
        $chunk->setMaxExecutionInSeconds($input->getOption('execution'));
        $chunk->setDebug((bool)$input->getOption('debug'));

        $progressBar = null;

        if (!$chunk->isDebug()) {
            $progressBar = new ProgressBar($output, 100);
            $progressBar->setFormat('[%bar%] %percent:3s%% %elapsed:6s% %memory:6s%');
            $progressBar->start();
        }

        while(true) {
            $optionsLine = sprintf('--options=%s',base64_encode($chunk->toJson()));

            $commandArray = [];
            if (strlen($input->getOption('phpexecutable'))) {
                $commandArray[] = $input->getOption('phpexecutable');
            }
            $commandArray[] = sprintf('%sbin/console',strlen($input->getOption('approot'))?$input->getOption('approot'):'');
            $commandArray[] = ChunkJobCommand::getDefaultName();
            $commandArray[] = $optionsLine;

            if ($chunk->isDebug()) {
                $output->writeln(sprintf('Start process: "%s"',implode(' ',$commandArray)));
            }

            $process = new Process($commandArray);
            $process->start();

            while($process->isRunning()) {
                $progressBar && $progressBar->setProgress($this->getProgress($job));
                sleep(1);
            }

            $response = $process->getOutput();

            if (strlen($response)
                && json_decode($response) !== false
                && is_array(json_decode($response,true))
                && count(json_decode($response,true))) {
                $chunk = Chunk::fromJson($response);
            } else {
                $chunk = Chunk::fromArray();
                $chunk->setFailure(true);
                $chunk->addMessage(strlen($response)?$response:sprintf('Response from process: %s.',$process->getExitCodeText()));
            }

            if ($chunk->isFinish()) {
                $progressBar && $progressBar->setProgress(100);
                $progressBar && $progressBar->finish();
                $output->writeln('');
                $this->printChunkMessages($output,$chunk);
                return true;
            }

            if ($chunk->isFailure()) {
                $progressBar && $progressBar->setProgress(100);
                $progressBar && $progressBar->finish();
                $output->writeln('');
                $this->printChunkMessages($output,$chunk);
                $job->setError($chunk->getMessagesAsString());
                return false;
            }

            if ($chunk->isDebug()) {
                $output->writeln(sprintf('Progress %s/%s',$chunk->getCounter(),$chunk->getMaxCounter()));
                $output->writeln(sprintf('Stage: %s',$chunk->getStage()));
                $output->writeln(sprintf('Memory: %s MB',(int)(memory_get_peak_usage() / 1024 / 1024)));
                $this->printChunkMessages($output,$chunk);
                $this->writeSeparator($output);
            }
            $chunk->setMessages([]);
        }
    }

    /**
     * @param $output
     */
    protected function writeSeparator($output): void
    {
        $output->writeln('--------------------------------------------------------');
    }

    /**
     * @param OutputInterface $output
     * @param \BAFzA\AntragBundle\Dto\Export\Transfer\Chunk $chunk
     */
    protected function printChunkMessages(OutputInterface $output, Chunk $chunk): void
    {
        $lines = $chunk->getMessagesRaw();
        if (is_array($lines) && count($lines)) {
            foreach ($lines as $line) {
                $output->writeln($line);
            }
        }
    }

    /**
     * @param Job $job
     * @return int
     */
    protected function getProgress(Job $job): int
    {
        try {

            $statement =
                $this->em
                    ->getConnection()
                    ->executeQuery(sprintf('SELECT progress from chunked_jobs c WHERE id = %s;',$job->getId()));
            $result =
                $statement->fetch();

            return (int)$result['progress'];

        } catch (\Exception | \Throwable $e) {
        }

        return 0;
    }
}
