<?php

namespace ChunkedBatch\ChunkedbatchBundle\Command;

use ChunkedBatch\ChunkedbatchBundle\Dto\Chunk;
use ChunkedBatch\ChunkedbatchBundle\Factory\JobFactory;
use ChunkedBatch\ChunkedbatchBundle\Service\ChunkHandler;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\HttpKernel\KernelInterface;

/**
 * Class ChunkJobCommand
 * @package ChunkedBatch\ChunkedbatchBundle\Command
 */
class ChunkJobCommand extends Command
{
    /**
     * @var string
     */
    protected static $defaultName = 'chunked:single_job:run';

    /**
     * @var JobFactory
     */
    protected $factory;

    /**
     * @var ChunkHandler
     */
    protected $chunkHandler;

    /**
     * ChunkJobCommand constructor.
     * @param JobFactory $factory
     * @param ChunkHandler $chunkHandler
     */
    public function __construct(JobFactory $factory, ChunkHandler $chunkHandler)
    {
        parent::__construct();
        $this->factory = $factory;
        $this->chunkHandler = $chunkHandler;
    }

    /**
     *
     */
    protected function configure()
    {
        $this
            ->setHidden(true)
            ->setDescription('Executes job')
            ->addOption('options', 'o',InputOption::VALUE_REQUIRED, 'Options')
        ;
    }

    /**
     * @param InputInterface $input
     * @param OutputInterface $output
     * @return int|null
     * @throws \Exception
     */
    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $chunk = null;

        try {

            $options = $input->getOption('options');
            $chunk = Chunk::fromJson(base64_decode($options));

            $this->chunkHandler->setMaxExecutionInSeconds((int)$chunk->getMaxExecutionInSeconds());
            $this->chunkHandler->setMaxMemoryUsageInBytes((int)$chunk->getMaxMemoryUsageInBytes());
            $this->chunkHandler->setDebug($chunk->isDebug());

            if (!$this->factory->existsType($chunk->getType())) {
                throw new \Exception(sprintf('Job with type "%s" unknown.',$chunk->getType()));
            }

            $chunk =
                $this->chunkHandler->mainChunk(
                  $this->factory->getByType($chunk->getType()),
                  $chunk
                );


        } catch(\Throwable | \Exception $e) {

            if (!($chunk instanceof Chunk)) {
                $chunk = Chunk::fromArray();
            }

            $chunk->setFailure(true);
            $chunk->addMessage($e->getMessage());
            $chunk->addMessage($e->getLine());
            $chunk->addMessage($e->getFile());

        }

        $output->write($chunk->toJson());

        return 0;
    }
}
