<?php

namespace ChunkedBatch\ChunkedbatchBundle\Command;

use ChunkedBatch\ChunkedbatchBundle\Entity\Job;
use ChunkedBatch\ChunkedbatchBundle\Factory\JobFactory;
use ChunkedBatch\ChunkedbatchBundle\Service\ChunkHandler;
use Doctrine\Persistence\ManagerRegistry;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;


/**
 * Class AddChunkJobCommand
 * @package ChunkedBatch\ChunkedbatchBundle\Command
 */
class AddChunkJobCommand extends Command
{
    /**
     * @var string
     */
    protected static $defaultName = 'chunked:jobs:add';

    /**
     * @var JobFactory
     */
    protected $factory;

    /**
     * @var ManagerRegistry
     */
    protected $doctrine;

    /**
     * AddChunkJobCommand constructor.
     */
    public function __construct(ManagerRegistry $doctrine, JobFactory $factory)
    {
        parent::__construct();

        $this->doctrine = $doctrine;
        $this->factory = $factory;
    }

    /**
     *
     */
    protected function configure()
    {
        $this
            ->setDescription('Add a job to the queue.')
            ->addArgument('type', InputArgument::REQUIRED)
        ;
    }

    /**
     * @param InputInterface $input
     * @param OutputInterface $output
     * @return int|null
     * @throws \Exception
     */
    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $type= $input->getArgument('type');

        if (!$this->factory->existsType($type)) {

            $output->writeln(sprintf('Type "%s" unknown.',$type));
            return 1;
        }

        $job = new Job($type,'','command');
        $job->setType($type);
        $job->setStatus(Job::STATUS_NEW);

        $this->doctrine->getManager()->persist($job);
        $this->doctrine->getManager()->flush();

        $output->writeln('Done.');

        return 0;
    }
}
