<?php

namespace ChunkedBatch\ChunkedbatchBundle\DependencyInjection\Compiler;

use ChunkedBatch\ChunkedbatchBundle\Factory\JobFactory;
use Symfony\Component\DependencyInjection\Compiler\CompilerPassInterface;
use Symfony\Component\DependencyInjection\ContainerBuilder;
use Symfony\Component\DependencyInjection\Reference;

/**
 * Class JobControllerPass
 * @package ChunkedBatch\ChunkedbatchBundle\DependencyInjection\Compiler
 */
class JobControllerPass implements CompilerPassInterface
{
    const TAG = 'chunked_job';

    /**
     * You can modify the container here before it is dumped to PHP code.
     */
    public function process(ContainerBuilder $container)
    {
        $definition = $container->findDefinition(JobFactory::class);
        $taggedServices = $container->findTaggedServiceIds(self::TAG);

        foreach ($taggedServices as $id => $tags) {
            foreach ($tags as $tag) {
                $definition->addMethodCall('addJob', array(new Reference($id)));
            }
        }
    }
}
