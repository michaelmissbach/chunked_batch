<?php

namespace ChunkedBatch\ChunkedbatchBundle\DependencyInjection;

use ChunkedBatch\ChunkedbatchBundle\DependencyInjection\Compiler\JobControllerPass;
use ChunkedBatch\ChunkedbatchBundle\Interfaces\IChunkedJob;
use Symfony\Component\DependencyInjection\ContainerBuilder;
use Symfony\Component\Config\FileLocator;
use Symfony\Component\HttpKernel\DependencyInjection\Extension;
use Symfony\Component\DependencyInjection\Loader;

/**
 * Class ChunkedbatchExtension
 * @package BaseApp\BaseappBundle\DependencyInjection
 */
class ChunkedbatchExtension extends Extension
{
    /**
     * {@inheritdoc}
     */
    public function load(array $configs, ContainerBuilder $container)
    {
        $container->registerForAutoconfiguration(IChunkedJob::class)
            ->addTag(JobControllerPass::TAG)
            ->setPrivate(false)
            ->setPublic(true)
        ;

        (new Loader\YamlFileLoader($container, new FileLocator(__DIR__.'/../Resources/config')))->load('services.yaml');
    }
}
