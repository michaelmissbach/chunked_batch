<?php

namespace ChunkedBatch\ChunkedbatchBundle\Service;

use ChunkedBatch\ChunkedbatchBundle\Abstracts\ChunkedJob;
use ChunkedBatch\ChunkedbatchBundle\Builder\IndexBuilder;
use ChunkedBatch\ChunkedbatchBundle\Builder\ResponseBuilder;
use ChunkedBatch\ChunkedbatchBundle\Builder\StageBuilder;
use ChunkedBatch\ChunkedbatchBundle\Dto\Chunk;
use ChunkedBatch\ChunkedbatchBundle\Entity\Job;
use ChunkedBatch\ChunkedbatchBundle\Exception\EndOfInputFileException;
use ChunkedBatch\ChunkedbatchBundle\Exception\FinishChunkException;
use Doctrine\ORM\EntityManagerInterface;
use Doctrine\Persistence\ManagerRegistry;
use Symfony\Component\HttpFoundation\ParameterBag;
use Symfony\Component\HttpKernel\KernelInterface;

/**
 * Class ChunkHandler
 * @package ChunkedBatch\ChunkedbatchBundle\Service
 */
class ChunkHandler
{
    const FINAL_STAGE = 'final';

    const CACHE_BAG = 'cachebag';

    const LOG_FILE = 'log.txt';

    /**
     * @var int
     */
    protected $maxExecutionInSeconds = 120;

    /**
     * @var int
     */
    protected $maxMemoryUsageInBytes = 512000000;

    /**
     * @var bool
     */
    protected $debug = true;

    /**
     * @var Chunk
     */
    protected $chunk;

    /**
     * @var Job
     */
    protected $job;

    /**
     * @var ParameterBag
     */
    protected $cacheBag;

    /**
     * @var string
     */
    protected $tempChunkPath;

    /**
     * @var int
     */
    protected $startTime;

    /**
     * @var resource
     */
    protected $chunkInputFileHandle;

    /**
     * @var ChunkedJob
     */
    protected $chunkedJob;

    /**
     * @var KernelInterface
     */
    protected $kernel;

    /**
     * @var ManagerRegistry
     */
    protected $doctrine;

    /**
     * @var EntityManagerInterface
     */
    protected $em;

    /**
     * @var float
     */
    protected $lastUpdate;

    /**
     * interval in seconds
     * @var int
     */
    protected $interval = 1;

    /**
     * @var array
     */
    protected $stages;

    /**
     * ChunkHandler constructor.
     * @param KernelInterface $kernel
     * @param ManagerRegistry $doctrine
     */
    public function __construct(KernelInterface $kernel, ManagerRegistry $doctrine)
    {
        $this->kernel   = $kernel;
        $this->doctrine = $doctrine;
        $this->em       = $doctrine->getManager();
    }

    /**
     * @param int $maxExecutionInSeconds
     * @return ChunkHandler
     */
    public function setMaxExecutionInSeconds(int $maxExecutionInSeconds): ChunkHandler
    {
        $this->maxExecutionInSeconds = $maxExecutionInSeconds;
        return $this;
    }

    /**
     * @param int $maxMemoryUsageInBytes
     * @return ChunkHandler
     */
    public function setMaxMemoryUsageInBytes(int $maxMemoryUsageInBytes): ChunkHandler
    {
        $this->maxMemoryUsageInBytes = $maxMemoryUsageInBytes;
        return $this;
    }

    /**
     * @param bool $debug
     * @return ChunkHandler
     */
    public function setDebug(bool $debug): ChunkHandler
    {
        $this->debug = $debug;
        return $this;
    }

    /**
     * @param $message
     */
    protected function addDebugMessage($message): void
    {
        if ($this->debug) {
            $this->chunk->addMessage($message);
            $this->saveChunkDataFile(self::LOG_FILE,sprintf('%s%s',$message,PHP_EOL),true,false);
        }
    }

    /**
     * @param string $fileName
     * @return string
     */
    protected function generateChunkTempFilePath(string $fileName): string
    {
        return sprintf('%s%s',$this->tempChunkPath,$fileName);
    }

    /**
     * @param string $fileName
     * @return string
     */
    protected function loadChunkDataFile(string $fileName): string
    {
        $fileName = $this->generateChunkTempFilePath($fileName);
        $this->addDebugMessage(sprintf('LoadChunkDataFile: %s',$fileName));
        $content = file_get_contents($fileName);
        return $content ?:'';
    }

    /**
     * @param string $fileName
     * @param string $content
     * @param bool $append
     * @param bool $debug
     */
    protected function saveChunkDataFile(string $fileName, string $content, bool $append = false, bool $debug = true): void
    {
        $fileName = $this->generateChunkTempFilePath($fileName);
        if ($debug) {
            $this->addDebugMessage(sprintf('SaveChunkDataFile: %s',$fileName));
        }
        file_put_contents($fileName,$content,$append ? FILE_APPEND : 0);
    }

    /**
     * @param string $fileName
     */
    protected function removeChunkDataFile(string $fileName): void
    {
        if ($this->chunkFileExists($fileName)) {
            $fileName = $this->generateChunkTempFilePath($fileName);
            $this->addDebugMessage(sprintf('RemoveChunkDataFile: %s',$fileName));
            unlink($fileName);
        }
    }

    /**
     * @param string $fileName
     * @return bool
     */
    protected function chunkFileExists(string $fileName): bool
    {
        $fileName = $this->generateChunkTempFilePath($fileName);
        $exists = file_exists($fileName);
        $this->addDebugMessage(sprintf('ChunkFileExists: Requested file %s does %sexists.',$fileName,$exists?'':'not '));
        return $exists;
    }

    /**
     * Entry Point
     *
     * @param ChunkedJob $chunkedJob
     * @param Chunk $chunk
     * @param string $path
     * @return Chunk
     */
    public function mainChunk(ChunkedJob $chunkedJob, Chunk $chunk): Chunk
    {
        $this->chunkedJob = $chunkedJob;
        $this->chunk = $chunk;
        $this->tempChunkPath = sprintf('%s/%s/%s/',$this->kernel->getProjectDir() ,'var/temp',$this->chunkedJob::getType());

        $stageBuilder = StageBuilder::create();
        $this->chunkedJob->getChunkStages($stageBuilder);
        $this->stages = $stageBuilder->getAll();

        try {
            $this->addDebugMessage('----- Stack trace ------');

            if (is_null($this->chunk->getStage())) {
                $this->addDebugMessage('Enter cleanUpChunk.');
                $this->cleanUpChunk();
            }

            $this->addDebugMessage('Enter initializeChunk.');
            $this->initializeChunk();

            $this->addDebugMessage('Enter mainChunkLoop.');
            $this->mainChunkLoop();

        } catch(FinishChunkException $e) {
            $this->setFinish();
            $this->chunkedJob->afterSetFinish();
        } catch(\Exception | \Throwable $e) {
            $this->setFailure($e);
        } finally {
            $this->addDebugMessage('Enter shutDownChunk.');
            if ($this->chunk->getCounter() > 0) {
                $this->addDebugMessage(
                    sprintf(
                        'Finish Progress %s/%s',
                        $this->chunk->getCounter() > $this->chunk->getMaxCounter() ? $this->chunk->getMaxCounter() : $this->chunk->getCounter(),
                        $this->chunk->getMaxCounter()
                    )
                );
                $this->addDebugMessage(sprintf('Stage: %s',$chunk->getStage()));
            }
            $this->addDebugMessage(
                sprintf('Current memory: %s MB, current execution time: %s s.',
                    (int)(memory_get_peak_usage() / 1024 / 1024),
                    time() - $this->startTime)
            );
            $this->addDebugMessage('----- End stack trace ------');
            $this->shutDownChunk();
        }

        return $this->chunk;
    }

    /**
     * @param $e
     */
    protected function setFailure($e): void
    {
        $this->chunk->addMessage('Error occured:');
        $this->chunk->addMessage($e->getMessage());
        $this->chunk->addMessage(sprintf('in File %s',$e->getFile()));
        $this->chunk->addMessage(sprintf('at Line %s',$e->getLine()));
        $this->chunk->setFailure(true);
    }

    /**
     *
     */
    public function cleanUpChunk(): void
    {
        $this->removeChunkDataFile(self::LOG_FILE);
        $this->removeChunkDataFile(self::CACHE_BAG);

        foreach ($this->stages as $stage) {
            $this->removeChunkDataFile(sprintf('%s.stg',strtolower($stage)));
        }
        $this->removeChunkDataFile(sprintf('%s.stg',strtolower(self::FINAL_STAGE)));
    }

    /**
     * @throws \Exception
     */
    protected function initializeChunk(): void
    {
        if (!is_writable($this->tempChunkPath)) {
            if (!mkdir($this->tempChunkPath, 0777, true)) {
                throw new \RuntimeException(sprintf('Directory "%s" was not created', $this->tempChunkPath));
            }
        }

        $this->job = $this->em->getRepository(Job::class)->find($this->chunk->getJobId());
        if(!$this->job) {
            throw new \Exception(sprintf('Job with id %s not found.',$this->chunk->getJobId()));
        }

        if ($this->chunkFileExists(self::CACHE_BAG)) {
            $content = $this->loadChunkDataFile(self::CACHE_BAG);
            if ($content) {
                $this->cacheBag = unserialize($content);
            }
        }
        if (!($this->cacheBag instanceof ParameterBag)) {
            $this->cacheBag = new ParameterBag();
        }

        $this->startTime = time();

        if (!count($this->stages)) {
            throw new \Exception('No stages defined!');
        }
        foreach ($this->stages as $stage) {
            if (!method_exists($this->chunkedJob,$stage)) {
                throw new \Exception(sprintf('Method with name "%s" defined but not existing.',$stage));
            }
        }
    }

    /**
     *
     */
    protected function shutDownChunk(): void
    {
        if ($this->chunkInputFileHandle) {
            $this->closeCurrentInputStageChunkFile();
        }
        $this->em->persist($this->job);
        $this->em->flush();

        $this->saveChunkDataFile(self::CACHE_BAG,serialize($this->cacheBag));
    }

    /**
     * @param $currentStage
     * @return mixed
     * @throws \Exception
     */
    protected function getNextStage($currentStage)
    {
        foreach($this->stages as $key=>$stage) {
            if ($stage == $currentStage) {
                if (isset($this->stages[((int)$key)+1])) {
                    return ['counter'=>((int)$key)+1,'name'=>$this->stages[((int)$key)+1]];
                } else {
                    return ['counter'=>-1,'name'=>self::FINAL_STAGE];
                }
            }
        }
        throw new \Exception(sprintf('Stage %s not found.',$currentStage));
    }

    /**
     *
     */
    protected function closeCurrentInputStageChunkFile(): void
    {
        fclose($this->chunkInputFileHandle);
        $this->chunkInputFileHandle = null;
    }

    /**
     * @return mixed
     * @throws EndOfInputFileException
     */
    protected function getCurrentInputStageChunkLine()
    {
        if (!$this->chunkInputFileHandle) {
            $this->chunkInputFileHandle = fopen($this->generateChunkTempFilePath(strtolower(sprintf('%s.%s',$this->chunk->getStage(),'stg'))),'r');

            $this->countCurrentInputStageChunkLine();
            $this->forwardToCurrentInputStageChunkLine();
        }

        $result = fgets($this->chunkInputFileHandle);
        if ($result === false) {
            $this->closeCurrentInputStageChunkFile();
            throw new EndOfInputFileException();
        }
        $this->chunk->iterateCounter();
        return unserialize($result);
    }

    /**
     *
     */
    protected function countCurrentInputStageChunkLine(): void
    {
        $linecount = 0;
        while(!feof($this->chunkInputFileHandle)) {
            if(fgets($this->chunkInputFileHandle)) {
                $linecount++;
            }
        }
        rewind($this->chunkInputFileHandle);
        $this->chunk->setMaxCounter($linecount);
    }

    /**
     * @throws EndOfInputFileException
     */
    protected function forwardToCurrentInputStageChunkLine(): void
    {
        if ($this->chunk->getCounter() > 0) {
            $tmpCounter = 0;
            while($tmpCounter < $this->chunk->getCounter()) {
                $result = fgets($this->chunkInputFileHandle);
                if ($result === false) {
                    $this->closeCurrentInputStageChunkFile();
                    throw new EndOfInputFileException();
                }
                $tmpCounter++;
            }
        }
    }

    /**
     * @param array $content
     * @throws \Exception
     */
    protected function writeCurrentOutputStageChunkLine($content,$stage = null): void
    {
        if (!$stage) {
            $stage = $this->getNextStage($this->chunk->getStage());
        }
        $fileName = $this->generateChunkTempFilePath(strtolower(sprintf('%s.%s',$stage['name'],'stg')));
        file_put_contents($fileName,sprintf('%s%s',serialize($content),PHP_EOL),FILE_APPEND);
    }

    /**
     * isLastStage
     *
     * @param  mixed $current
     * @return bool
     */
    protected function isLastStage($current): bool
    {
        return $current == end($this->stages);
    }

    /**
     * @throws \Exception
     */
    protected function mainChunkLoop(): void
    {
        $this->addDebugMessage(
            sprintf('Current memory: %s MB, current execution time: %s s.',
                (int)(memory_get_peak_usage() / 1024 / 1024),
                time() - $this->startTime)
        );

        if (is_null($this->chunk->getStage())) {
            $this->handleStartStages();
            return;
        }

        $responseBuilder = ResponseBuilder::create();

        do {
            try {
                $responseBuilder->reset();
                $currentLine = $this->getCurrentInputStageChunkLine();

                $this->addDebugMessage(
                    sprintf('Execute progress %s/%s on Stage "%s"',
                        $this->chunk->getCounter(),
                        $this->chunk->getMaxCounter(),
                        $this->chunk->getStage()
                    )
                );

                $method = $this->chunk->getStage();

                $this->chunkedJob->$method($responseBuilder, $currentLine);

                if ($responseBuilder->count() && $this->isLastStage($this->chunk->getStage())) {
                    throw new \Exception(sprintf('"%s" is the last defined stage. Returning a response is illegal.',$this->chunk->getStage()));
                }

                if ($responseBuilder->count()) {
                    $this->addDebugMessage(' -> Result for next stage added.');
                    $result = $responseBuilder->getAll();
                    $this->writeCurrentOutputStageChunkLine($result);
                }

                if ($responseBuilder->isExitStage()) {
                    $this->addDebugMessage(' -> Exit stage signaled.');
                    throw new EndOfInputFileException();
                }

                $this->handleProgress();

            } catch(EndOfInputFileException $e) {

                $this->handleProgress(true);

                $this->chunk->iterateCounter();

                $nextStage = $this->getNextStage($this->chunk->getStage());
                if ($nextStage['name'] === self::FINAL_STAGE) {
                    throw new FinishChunkException();
                }

                $this->chunk->setStage($nextStage['name']);
                $this->chunk->setStageNumber((int)$nextStage['counter']+1);
                $this->chunk->setCounter(0);
                return;

            } catch(\Exception | \Throwable $e) {

                throw $e;

            }
        } while($this->noNeedToExitChunk());
    }

    /**
     * @throws FinishChunkException
     */
    protected function handleStartStages(): void
    {
        $this->addDebugMessage('Stage: chunkStageIndex');

        $this->chunkedJob->beforeChunkStageIndex();
        $indexBuilder = IndexBuilder::create();
        $this->chunkedJob->chunkStageIndex($indexBuilder);
        $this->chunkedJob->afterChunkStageIndex($result);

        if (!$indexBuilder->count()) {
            throw new FinishChunkException();
        }

        $this->chunk->setStage($this->stages[0]);
        $this->chunk->setStageNumber(1);
        foreach ($indexBuilder->getYielded() as $line) {
            $this->writeCurrentOutputStageChunkLine($line,['name'=>$this->chunk->getStage()]);
        }
    }

    /**
     * @param bool $forceUpdate
     */
    protected function handleProgress($forceUpdate = false): void
    {
        $parts = 100 / count($this->stages);
        $min = ($this->chunk->getStageNumber() - 1) * $parts;
        $max = (($this->chunk->getStageNumber() - 1) * $parts) + $parts;

        $this->addProgress($this->chunk->getCounter(),$this->chunk->getMaxCounter(),$min,$max,$forceUpdate);
    }

    /**
     * @return bool
     */
    protected function noNeedToExitChunk(): bool
    {
        if ((time() - $this->startTime) > $this->maxExecutionInSeconds) {
            $this->addDebugMessage(sprintf('Chunk exits because of execution time (%s s)', $this->maxExecutionInSeconds));
            return false;
        }

        $usedMemory = memory_get_peak_usage();

        $maxMemory = $this->getMemoryLimitInBytes();
        if ($maxMemory > -1) {
            $maxMemory = $maxMemory * 0.8;
            if ($usedMemory > $maxMemory) {
                $this->addDebugMessage(
                    sprintf('Chunk exits because of %s of max ini_set memory (%s MB), actual used (%s MB)',
                        '80%',
                        (int)($maxMemory / 1024 / 1024),
                        (int)($usedMemory / 1024 / 1024)
                    )
                );
                return false;
            }
        }

        if ($usedMemory > $this->maxMemoryUsageInBytes) {
            $this->addDebugMessage(
                sprintf('Chunk exits because of max memory (%s MB), actual used (%s MB)',
                    (int)($this->maxMemoryUsageInBytes / 1024 / 1024),
                    (int)($usedMemory / 1024 / 1024)
                )
            );
            return false;
        }

        return true;
    }

    /**
     * @return int
     */
    protected function getMemoryLimitInBytes(): int
    {
        try {
            $maxMemory = ini_get('memory_limit');
            if (preg_match('/^(\d+)(.)$/', $maxMemory, $matches)) {
                if ($matches[2] == 'G') {
                    $maxMemory = $matches[1] * 1024 * 1024 * 1024; // nnnM -> nnn GB
                } else if ($matches[2] == 'M') {
                    $maxMemory = $matches[1] * 1024 * 1024; // nnnM -> nnn MB
                } else if ($matches[2] == 'K') {
                    $maxMemory = $matches[1] * 1024; // nnnK -> nnn KB
                } else if ($matches[2] == 'B') {
                    $maxMemory = $matches[1]; // nnnK -> nnn B
                } else {
                    $maxMemory = -1;
                }
            }
            return (int)$maxMemory;
        } catch (\Exception $e) {}

        return -1;
    }

    /**
     *
     */
    protected function setFinish(): void
    {
        $this->job->setProgress(100);
        $this->chunk->setFinish(true);
    }

    /**
     * @param $currentValue
     * @param $maxValue
     * @param int $relativeMin
     * @param int $relativeMax
     * @param bool $forceUpdate
     * @return int|null
     */
    protected function addProgress($currentValue,$maxValue,$relativeMin = 0,$relativeMax = 100, $forceUpdate = false): ?int
    {
        $relativePercent = 0;

        if (!$forceUpdate) {
            if (!$this->chunk->getJobId()) {
                return null;
            }
            if (is_null($this->lastUpdate)) {
                $this->lastUpdate = microtime(true);
            }
            if ($this->lastUpdate + $this->interval > microtime(true)) {
                return null;
            }
        }

        try {
            $relativePercent = $relativeMin + (((($currentValue / $maxValue) * 100) * ($relativeMax - $relativeMin)) / 100);

            $this->em->getConnection()
                ->executeUpdate('UPDATE chunked_jobs SET progress = ? WHERE id = ?', [$relativePercent,$this->chunk->getJobId()]);

        } catch(\Throwable | \Exception $e) {
        }

        $this->lastUpdate = microtime(true);

        return (int)$relativePercent;
    }
}
