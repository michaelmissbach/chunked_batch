<?php

namespace ChunkedBatch\ChunkedbatchBundle\Interfaces;

use ChunkedBatch\ChunkedbatchBundle\Builder\IndexBuilder;
use ChunkedBatch\ChunkedbatchBundle\Builder\StageBuilder;

/**
 * Interface IChunkedJob
 * @package ChunkedBatch\ChunkedbatchBundle\Interfaces
 */
interface IChunkedJob
{
    /**
     * @return string
     */
    public static function getType(): string;

    /**
     * @param StageBuilder $stageBuilder
     */
    public function getChunkStages(StageBuilder $stageBuilder): void;

    /**
     * @param IndexBuilder $indexBuilder
     */
    public function chunkStageIndex(IndexBuilder $indexBuilder): void;
}
