<?php

namespace ChunkedBatch\ChunkedbatchBundle\Builder;

use ChunkedBatch\ChunkedbatchBundle\Abstracts\AbstractBuilder;

/**
 * Class ResponseBuilder
 * @package ChunkedBatch\ChunkedbatchBundle\Builder
 */
class ResponseBuilder extends AbstractBuilder
{    
    /**
     * exitStage
     *
     * @var bool
     */
    protected $exitStage = false;

    /**
     * reset
     *
     * @return void
     */
    public function reset(): void
    {
        $this->clear();
        $this->exitStage = false;
    }

    /**
     * Get exitStage
     *
     * @return  bool
     */ 
    public function isExitStage(): bool
    {
        return $this->exitStage;
    }

        
    /**
     * exitStage
     *
     * @return ResponseBuilder
     */
    public function exitStage(): ResponseBuilder
    {
        $this->exitStage = true;

        return $this;
    }
}
