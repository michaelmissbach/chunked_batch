<?php

namespace ChunkedBatch\ChunkedbatchBundle\Builder;

use ChunkedBatch\ChunkedbatchBundle\Abstracts\AbstractBuilder;

/**
 * Class StageBuilder
 * @package ChunkedBatch\ChunkedbatchBundle\Builder
 */
class StageBuilder extends AbstractBuilder
{
    /**
     * @param $payload
     * @return bool
     */
    public function add($payload): bool
    {
        !is_array($payload) && $payload = [$payload];

        foreach ($payload as $p) {
            parent::add($p);
        }

        return true;
    }
}
